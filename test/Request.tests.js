module.exports = {

	setUp: function (callback) {
		this.request = require('request');
		this.underscore = require('underscore');
		// this.qs = require('querystring');


		callback();
	},
	tearDown: function (callback) {
		// clean up
		delete this.request;
		delete this.underscore;
		// delete this.qs;

		callback();
	},
	"test PUT record to CouchDB - should get back same ID": function(test){
		test.expect(3);

		var rand = Math.floor(Math.random()*100000000).toString();
		var _ = this.underscore;

		this.request(
			{ method: 'PUT'
				, uri: 'http://mikeal.iriscouch.com/testjs/' + rand
				, multipart:
				[ { 'content-type': 'application/json'
					,  body: JSON.stringify({foo: 'bar', _attachments: {'message.txt': {follows: true, length: 18, 'content_type': 'text/plain' }}})
				}
					, { body: 'I am an attachment' }
				]
			}
			, function (error, response, body) {


				if(response.statusCode == 201){

					console.log('document saved as: http://mikeal.iriscouch.com/testjs/'+ rand);
					// process.stdout.write('document saved as: http://mikeal.iriscouch.com/testjs/'+ rand);

					var responseJSON = JSON.parse(body);

					test.ok(responseJSON.ok === true, "responseJSON.ok is true");
					test.equal(responseJSON.id, rand, "responseJSON.id should be same as we set it");
					test.ok(_.has(responseJSON, "rev"), "responseJSON has property 'rev'");

					test.done();

				} else {

					console.log('error: '+ response.statusCode);
					console.log(body);

				}
			}
		);

	}


};