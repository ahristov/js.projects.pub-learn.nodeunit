# Nodeunit

"Simple syntax, powerful tools.
**Nodeunit** provides easy async unit testing
for node.js and the browser."

> URL: https://github.com/caolan/nodeunit


## Installation


You can install the `nodeunit` and `request` using `npm`:

	npm install nodeunit -g
	npm install request -g

you can then run:

	$ nodeunit

Note: See next section on how to add node
modules dependencies in your project
so that you can distribute them together
with your code and no need to
install on the client mashine.


## Setup dependencies in project's git repository

From the project's home directory add
all the node modules the project depends on:

	git submodule add git://github.com/caolan/nodeunit.git node_modules/nodeunit
	git submodule add git://github.com/documentcloud/underscore.git node_modules/underscore
	git submodule add git://github.com/maritz/node-sprintf.git node_modules/sprintf
	git submodule add git://github.com/mikeal/request.git node_modules/request
	git submodule add git://github.com/jazzychad/querystring.node.js.git node_modules/querystring

Now, when cloning the repository, nodeunit can be downloaded by doing the following:

	git submodule init
	git submodule update

Note: To remove a submodule from git repository:

- Delete the relevant line from the .gitmodules file.
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).

Example:

	git rm --cached node_modules/shred

- Commit to git
- Delete the now untracked submodule files.


You can now run `nodeunit` from the project's home directory:

	$ node node_modules\nodeunit\bin\nodeunit
	Files required.
	Usage: nodeunit [options] testmodule1.js testfolder [...]
	Options:

	  --config FILE     the path to a JSON file with options
	  --reporter FILE   optional path to a reporter file to customize the output
	  --list-reporters  list available build-in reporters
	  -t name,          specify a test to run
	  -h, --help        display this help and exit
	  -v, --version     output version information and exit


## Project structure

Let's follow this convention

	README.md			this file
	node_modules		contains node modules relevant for the project
	reports				the jUnit compatable reports after running the tests
	run-tests.bat		use from Hudson to run the tests
	run-tests.js		you can run the tests with this too
	test				contains the unit tests



## Running the tests

Running the tests from command prompt:


	$ node run-tests.js

	Request.tests
	document saved as: http://mikeal.iriscouch.com/testjs/53634608
	✔ test PUT record to CouchDB - should get back same ID

	OK: 3 assertions (1522ms)



## Links

[nodeunit](https://github.com/caolan/nodeunit "link to nodeunit")

[request](https://github.com/mikeal/request "link to shred")

[Blog post](http://cjohansen.no/en/node_js/unit_testing_node_js_apps)

[Blog post](http://www.ipreferjim.com/2011/08/node-js-getting-started-with-nodeunit/)



